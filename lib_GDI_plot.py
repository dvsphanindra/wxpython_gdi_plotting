import wx

import numpy as np

line_colour1 = '#950111'
line_colour2 = '#1ac500'

class wxgrid():
	def __init__(self, plotPanel, x0=None, y0=None, width=None, height=None):
		self.panel_grid = plotPanel
		self.panel_gridDC = wx.PaintDC(plotPanel)
		
		self.panel_size = self.panel_grid.GetSize()
		if x0 is None:
			self.x0 = self.panel_size[0] // 5
		else:
			self.x0 = x0
		if y0 is None:
			self.y0 = self.panel_size[1] // 6
		else:
			self.y0 = y0
		if width is None:
			self.width = self.panel_size[0] // 2
		else:
			self.width = width
		if height is None:
			self.height = int(self.panel_size[1] // 1.5)
		else:
			self.height = height
		
		self.noOfRows = 6
		self.noOfColumns = 9
		
		self.rowSpacing = self.height / self.noOfRows
		self.columnSpacing = self.width / self.noOfColumns
		
		self.circleSize = 3
		
		self.plotGrid()
	
	def plotGrid(self):
		# Recalculate for new data
		self.rowSpacing = self.height / self.noOfRows
		self.columnSpacing = self.width / self.noOfColumns
		
		self.panel_gridDC.SetPen(wx.Pen(line_colour1))
		self.panel_gridDC.DrawRectangle(self.x0, self.y0, self.width, self.height)
		for c in range(1,self.noOfColumns):
			self.panel_gridDC.DrawLine(self.x0 + self.columnSpacing * c, self.y0, self.x0 + self.columnSpacing * c, self.y0 + self.height)
		for r in range(1,self.noOfRows):
			self.panel_gridDC.DrawLine(self.x0, self.y0 + self.rowSpacing * r, self.x0 + self.width, self.y0 + self.rowSpacing * r)
	
	def drawPlot(self, x, y1=None, y2=None):
		if y1 is None:
			y1 = np.array(x, dtype=float)
			x = np.arange(len(y1))
		else:
			x = np.array(x, dtype=float)
			y1 = np.array(y1, dtype=float)
			
		min_y1 = min(y1)
		max_y1 = max(y1)
		len_y1 = len(y1)
		self.noOfColumns = len_y1 - 1
		spacingFactor_y1 = max_y1 / self.noOfRows
		
		min_x = min(x)
		max_x = max(x)
		len_x = len(x)
		
		assert (len_x == len_y1), "Arrays x, y must be of equal length len(x) = {0}, len(y) = {1}".format(str(len_x), str(len_y1))
		plot_x = self.x0 + self.width * (x - min_x) / (max_x - min_x)
		plot_y1 = self.y0 - self.height * (y1 - min_y1) / (max_y1 - min_y1) + self.height
		
		self.plotGrid()
		
		self.panel_gridDC.DrawText(str(min_y1), self.x0 - self.columnSpacing * 0.3, self.y0 + self.height)
		self.panel_gridDC.DrawText(str(max_y1), self.x0 - self.columnSpacing * 0.4, self.y0 - 10)
		self.panel_gridDC.SetPen(wx.Pen(line_colour1))
		
		for (x_point, y_point) in zip(plot_x, plot_y1):
			self.panel_gridDC.DrawCircle(x_point,y_point,self.circleSize)
		for i in range(len_x-1):
			self.panel_gridDC.DrawLine(plot_x[i], plot_y1[i], plot_x[i+1], plot_y1[i+1])
		
		if y2 is not None:
			y2 = np.array(y2, dtype=float)
			min_y2 = min(y2)
			max_y2 = max(y2)
			len_y2 = len(y2)
			
			assert (len_x == len_y2), "Arrays x, y1 and y2 must be of equal lengths len(x) = {0}, len(y1) = {1}, len(y2) = {2}".format(str(len_x), str(len_y1), str(len_y2))
			max_y1 = max(y1)
			len_y1 = len(y1)
			
			plot_y2 = self.y0 - self.height * (y2 - min_y2) / (max_y2 - min_y2) + self.height
			self.panel_gridDC.SetPen(wx.Pen(line_colour2))
			for (x_point, y_point) in zip(plot_x, plot_y2):
				self.panel_gridDC.DrawCircle(x_point, y_point, self.circleSize)
				
			for i in range(len_x - 1):
				self.panel_gridDC.DrawLine(plot_x[i], plot_y2[i], plot_x[i + 1], plot_y2[i + 1])
		
	def clearPlot(self):
		self.plotGrid()
			
	def show_Legend(self):
		# TODO To mark legend
		# self.font = wx.Font(15, wx.ROMAN, wx.ITALIC, wx.BOLD)
		# self.panel_gridDC.SetFont(self.font)
		# self.panel_gridDC.SetPen(wx.Pen('#950111'))
		# self.panel_gridDC.DrawLine(self.x0 + self.width + self.columnSpacing * 0.5, self.y0 + self.rowSpacing, self.x0 + self.width + self.columnSpacing * 1, self.y0 + self.rowSpacing)
		# self.panel_gridDC.DrawText(":Encoder1", self.x0 + self.width + self.columnSpacing * 1, self.y0 + self.rowSpacing - 10)
		# self.panel_gridDC.SetPen(wx.Pen('#1ac500'))
		# self.panel_gridDC.DrawLine(self.x0 + self.width + self.columnSpacing * 0.5, self.y0 + self.rowSpacing * 2, self.x0 + self.width + self.columnSpacing * 1, self.y0 + self.rowSpacing * 2)
		# self.panel_gridDC.DrawText(":Encoder2", self.x0 + self.width + self.columnSpacing * 1, self.y0 + self.rowSpacing * 2 - 10)
		
		# self.font = wx.Font(20, wx.ROMAN, wx.ITALIC, wx.BOLD)
		# self.panel_gridDC.SetFont(self.font)
		# self.panel_gridDC.DrawText("Degree", self.x0 - self.columnSpacing * 1.7, self.y0 + self.rowSpacing * 2)
		# self.panel_gridDC.DrawText("Time", self.x0 + self.columnSpacing * 4, self.height + self.y0 + 10)
		pass
	
	def getSize(self):
		# TODO: Return the size of the plot
		return
	
	def gridOFF(self):
		# TODO: Switch off the grid
		pass
	
	def gridON(self):
		# TODO: Switch on the grid
		pass
	
	def toggleGrid(self):
		# TODO: Toggle the grid
		pass
	