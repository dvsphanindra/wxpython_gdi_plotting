# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class mainFrame
###########################################################################

class mainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"GDI plotting Demo", pos = wx.DefaultPosition, size = wx.Size( 500,418 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.panel_grid = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1.Add( self.panel_grid, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel2 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

		self.toggleButton_Plot1 = wx.ToggleButton( self.m_panel2, wx.ID_ANY, u"Show Plot1", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.toggleButton_Plot1, 0, wx.ALL, 5 )

		self.toggleButton_Plot2 = wx.ToggleButton( self.m_panel2, wx.ID_ANY, u"Show Plot2", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.toggleButton_Plot2, 0, wx.ALL, 5 )


		self.m_panel2.SetSizer( bSizer2 )
		self.m_panel2.Layout()
		bSizer2.Fit( self.m_panel2 )
		bSizer1.Add( self.m_panel2, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.panel_grid.Bind( wx.EVT_PAINT, self.panel_gridDC_OnPaint )
		self.toggleButton_Plot1.Bind( wx.EVT_TOGGLEBUTTON, self.toggleButton_Plot1_OnToggle )
		self.toggleButton_Plot2.Bind( wx.EVT_TOGGLEBUTTON, self.toggleButton_Plot2_OnToggle )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def panel_gridDC_OnPaint( self, event ):
		event.Skip()

	def toggleButton_Plot1_OnToggle( self, event ):
		event.Skip()

	def toggleButton_Plot2_OnToggle( self, event ):
		event.Skip()


