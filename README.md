# wxPython GDI based plotting software

This is a wxPython GDI (Graphical Device Interface) based plotting software.

## Description

* This is a simple plotting software developed using wxPython GDI. It was developed as the plotting libraries such as matplotlib etc are heavy and take a significant amount of time for plot animation. This library is based on GDI in wxPython and is intented to be a light-weight plotting tool.
* Version 0.1

## How do I get set up?

This code has the following package dependencies:
* wxpython
* 

### via pip

```sudo pip install wxpython```

### via [Anaconda](https://www.continuum.io/what-is-anaconda) distribution
If you had installed Anaconda, you can install the above dependencies using the `conda` command.
```
conda install wxpython
```

## Contribution guidelines

This software is uploaded **AS IS**. Currently, there is no documentation available. We have not stuck to PEP8 guidelines.
You are free to contribute in whichever way you feel like such as, adding a feature, documentation, making the code compatible to PEP8 guidelines or anything else you can think of that will make the software useful to the community.

## Disclaimer

This software has been uploaded **AS IS**. Though it was used on Linux and Windows, there is no guarantee that this software is bug-free. Use with caution.

## License
![Alt text](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

