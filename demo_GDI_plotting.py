import wx
import demo_GDI_plotting_GUI
import lib_GDI_plot


class grid(demo_GDI_plotting_GUI.mainFrame):
	def __init__(self, parent):
		demo_GDI_plotting_GUI.mainFrame.__init__(self, parent)
		self.plotPanel = None
		self.plotObject = None
		
		self.x = range(15)
		self.plot1 = [0, 50, 100, 200, 250, 300, 350, 100, 200, 250, 300, 100, 400, 450, 600]
		self.plot2 = range(15)
		
	def panel_gridDC_OnPaint(self, event):
		self.plotPanel = event.GetEventObject()
		x=50;y=50
		self.plotObject = lib_GDI_plot.wxgrid(self.plotPanel)#,x,y)
		
		# data1 = [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 650, 600, 500, 550, 400, 450, 300, 350, 200, 250, 100, 50, 200, 700]
	
	def toggleButton_Plot1_OnToggle(self, event):
		if self.toggleButton_Plot1.GetValue():
			if self.toggleButton_Plot2.GetValue():
				self.plotObject.drawPlot(self.x, self.plot1, self.plot2)
			else:
				self.plotObject.drawPlot(self.x, self.plot1)
			self.toggleButton_Plot1.SetLabel("Hide Plot1")
		else:
			if self.toggleButton_Plot2.GetValue():
				self.plotObject.drawPlot(self.x, self.plot2)
			else:
				self.plotObject.clearPlot()
			self.toggleButton_Plot1.SetLabel("Show Plot1")
			
	def toggleButton_Plot2_OnToggle(self, event):
		if self.toggleButton_Plot2.GetValue():
			if self.toggleButton_Plot1.GetValue():
				self.plotObject.drawPlot(self.x, self.plot1, self.plot2)
			else:
				self.plotObject.drawPlot(self.x, self.plot2)
			self.toggleButton_Plot2.SetLabel("Hide Plot2")
		else:
			if self.toggleButton_Plot1.GetValue():
				self.plotObject.drawPlot(self.x, self.plot1)
			else:
				self.plotObject.clearPlot()
			self.toggleButton_Plot2.SetLabel("Show Plot2")
		


app = wx.App(False)
sa = grid(None)
sa.Show(True)
app.MainLoop()
